package net.therap.mealmanagement.domain;

/**
 * @author shahriar
 * @since 2/11/18
 */
public enum Slot {
    BREAKFAST, LUNCH
}
