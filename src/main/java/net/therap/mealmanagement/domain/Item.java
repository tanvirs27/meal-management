package net.therap.mealmanagement.domain;

import java.io.Serializable;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String name;

    public Item(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "id: " + id + ", item_name: " + name;
    }
}
