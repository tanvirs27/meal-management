package net.therap.mealmanagement.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    private String day;
    private String slot;
    private List<Item> items;

    public Meal(String day, String slot) {
        this.day = day;
        this.slot = slot;
        this.items = new ArrayList<>();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String toString() {
        String string = "";
        string += day + " " + slot + "\n";

        for (Item item : items) {
            string += item + "\n";
        }

        return (items.size() > 0) ? string : string + "No item available\n";
    }
}
