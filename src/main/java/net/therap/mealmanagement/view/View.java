package net.therap.mealmanagement.view;

import net.therap.mealmanagement.domain.Meal;

import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */
public class View {

    public void printMeals(List<Meal> meals) {
        for (Meal meal : meals) {
            System.out.println(meal);
        }
    }

    public void printLine(String line) {
        System.out.println(line);
    }
}
