package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Day;
import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.Slot;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class MealDao {

    private List<Meal> allMeals;
    private ItemDao itemDao;

    public MealDao(Connection connection) {
        this.allMeals = new ArrayList<>();
        this.itemDao = new ItemDao(connection);
    }

    public List<Meal> getAllMeal() {
        for (Day day : Day.values()) {
            for (Slot slot : Slot.values()) {
                Meal meal = new Meal(day.name(), slot.name());
                meal.setItems(itemDao.getItems(day.name(), slot.name()));
                allMeals.add(meal);
            }
        }
        return allMeals;
    }

    public List<Meal> getMealBySlot(String day, String slot) {
        Meal meal = new Meal(day, slot);
        meal.setItems(itemDao.getItems(day, slot));
        allMeals.add(meal);

        return allMeals;
    }
}
