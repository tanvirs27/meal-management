package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class ItemDao {

    private static final int ITEM_NOT_AVAILABLE = -1;

    private Connection connection;

    public ItemDao(Connection connection) {
        this.connection = connection;
    }

    public List<Item> getItems(String day, String slot) {

        ArrayList<Item> allItems = new ArrayList<>();

        PreparedStatement statement;

        String selectSQL = "SELECT meal_id, name " +
                "FROM meal_details " +
                "WHERE meal_id IN " +
                "(SELECT meal_id " +
                "FROM meal_day " +
                "WHERE day = ? " +
                "AND slot = ?)";

        try {
            statement = connection.prepareStatement(selectSQL);

            statement.setString(1, day);
            statement.setString(2, slot);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {

                int mealId = rs.getInt("meal_id");
                String mealName = rs.getString("name");

                Item item = new Item(mealId);
                item.setName(mealName);
                allItems.add(item);
            }

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        return allItems;
    }

    public void addItemToSlot(String day, String slot, String name) {
        PreparedStatement statement;

        String insertSQL = "INSERT INTO meal_day "
                + "(day, slot, meal_id) VALUES "
                + "(?, ?, ?)";

        try {
            statement = connection.prepareStatement(insertSQL);

            statement.setString(1, day);
            statement.setString(2, slot);
            statement.setInt(3, addItem(name));

            statement.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public void deleteItemFromSlot(String day, String slot, String name) {
        PreparedStatement statement;

        String deleteSQL = "DELETE FROM meal_day "
                + "WHERE day = ? AND slot = ? AND meal_id = ?";

        try {
            statement = connection.prepareStatement(deleteSQL);

            statement.setString(1, day);
            statement.setString(2, slot);
            statement.setInt(3, getItemId(name));

            statement.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public void updateItem(String day, String slot, String changedFrom, String changedTo) {
        PreparedStatement statement;

        int oldId = getItemId(changedFrom);
        int newId = addItem(changedTo);

        String updateSQL = "UPDATE meal_day "
                + "SET meal_id = ? "
                + "WHERE day = ? AND slot = ? AND meal_id = ?";

        try {
            statement = connection.prepareStatement(updateSQL);

            statement.setInt(1, newId);
            statement.setString(2, day);
            statement.setString(3, slot);
            statement.setInt(4, oldId);

            statement.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    private int addItem(String name) {

        int itemId = getItemId(name);

        if (itemId != ITEM_NOT_AVAILABLE) {
            return itemId;
        }

        PreparedStatement statement;

        String insertSQL = "INSERT INTO meal_details "
                + "(name) VALUES "
                + "(?)";

        try {
            statement = connection.prepareStatement(insertSQL);

            statement.setString(1, name);

            statement.executeUpdate();

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        return getItemId(name);
    }

    private int getItemId(String name) {

        PreparedStatement statement;

        String selectSQL = "SELECT meal_id " +
                "FROM meal_details " +
                "WHERE name = ?";

        try {
            statement = connection.prepareStatement(selectSQL);

            statement.setString(1, name);

            ResultSet rs = statement.executeQuery();

            if (rs.next()) {

                int id = rs.getInt("meal_id");

                return id;
            }

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        return ITEM_NOT_AVAILABLE;
    }
}
