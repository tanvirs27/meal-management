package net.therap.mealmanagement.app;

import net.therap.mealmanagement.controller.CommandParser;
import net.therap.mealmanagement.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class Main {

    private static final String INSTRUCTIONS = "Example command:\n" +
            "To view all: view all\n" +
            "To view a particular slot: view sunday breakfast\n" +
            "To add: add sunday lunch fish\n" +
            "To delete: delete sunday lunch beef\n" +
            "To update: update sunday lunch beef chicken\n";

    public static void main(String[] args) throws IOException {

        View view = new View();
        CommandParser controller = new CommandParser(view);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println(INSTRUCTIONS);

        while (true) {
            if (reader.ready()) {
                String command = reader.readLine();

                if (command.equals(CommandParser.EXIT_COMMAND)) {
                    break;
                }
                controller.parse(command);
            }
        }
        controller.closeDatabase();
    }
}
