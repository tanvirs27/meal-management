package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.ItemDao;
import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.util.DBConnector;
import net.therap.mealmanagement.view.View;

import java.sql.Connection;

/**
 * @author shahriar
 * @since 2/11/18
 */
public class CommandParser {

    public static final String VIEW_COMMAND = "view";
    public static final String VIEW_ALL = "all";
    public static final String ADD_COMMAND = "add";
    public static final String DELETE_COMMAND = "delete";
    public static final String UPDATE_COMMAND = "update";
    public static final String EXIT_COMMAND = "exit";

    public static final int MIN_COMMAND_LENGTH_FOR_VIEW = 3;
    public static final int MIN_COMMAND_LENGTH_FOR_ADD_DEL = 4;
    public static final int MIN_COMMAND_LENGTH_FOR_UPDATE = 5;

    public static final String ERROR_ARGUMENT = "Too few arguments";

    private DBConnector db;
    private Connection connection;
    private MealDao mealDao;
    private ItemDao itemDao;
    private View view;

    public CommandParser(View view) {
        this.db = new DBConnector();
        this.connection = db.getLocalConnection();
        this.mealDao = new MealDao(connection);
        this.itemDao = new ItemDao(connection);
        this.view = view;
    }

    public void parse(String command) {

        if (command.equals("")) {
            return;
        }

        String[] splittedCommand = command.toLowerCase().split(" ");

        String commandName = splittedCommand[0];

        switch (commandName) {
            case VIEW_COMMAND:
                if (splittedCommand[1].equals(VIEW_ALL)) {
                    view.printMeals(mealDao.getAllMeal());
                } else if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_VIEW) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    view.printMeals(mealDao.getMealBySlot(splittedCommand[1], splittedCommand[2]));
                }
                break;

            case ADD_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_ADD_DEL) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    itemDao.addItemToSlot(splittedCommand[1], splittedCommand[2], splittedCommand[3]);
                }
                break;

            case DELETE_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_ADD_DEL) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    itemDao.deleteItemFromSlot(splittedCommand[1], splittedCommand[2], splittedCommand[3]);
                }
                break;

            case UPDATE_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_UPDATE) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    itemDao.updateItem(splittedCommand[1], splittedCommand[2], splittedCommand[3], splittedCommand[4]);
                }
                break;

            default:
                break;
        }
    }

    public void closeDatabase() {
        db.closeConnection();
    }
}
