package net.therap.mealmanagement.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class DBConnector {

    private final static String HOST_NAME = "jdbc:mysql://localhost/";
    private final static String DB_NAME = "therap_meal";
    private final static String USER = "root";
    private final static String PASS = "rifat007";

    private Connection connection;

    public DBConnector() {
        this.connection = null;
    }

    public Connection getLocalConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(HOST_NAME + DB_NAME, USER, PASS);
        } catch (ClassNotFoundException e) {
            System.err.println("ClassNotFoundException in getConnection, " + e.getMessage());
        } catch (SQLException e) {
            System.err.println("SQLException in getConnection, " + e.getMessage());
        }

        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Unable to close connection");
        }
    }
}
