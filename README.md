#Assignment:#
Create a Java console application for maintaining weekly breakfast and lunch menu for our company. User can view & update weekly menu. Data will be persisted in database.

##Note:##
* Use only JDBC and SQL, no ORM framework is allowed
* Don’t use any NoSQL database
* No restriction in table design
